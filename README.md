# DM CRUD #

Petit projet étudiant visant à implémenter un *CRUD* avec le moteur de template *TWIG* (composant de Symphony2).

## Explication ##

Nous avons choisi la base de données **Films** pour réaliser ce travail.

Le routage est effectué dans le fichier *index.php*.
Le fichier *controller.php* choisi les actions à effectuer suivant les actions des utilisateurs.
Les fonctions qui interagissent avec la base de données sont dans le fichier *crud.php* (CREATE, RETRIEVE, UPDATE, DELETE).

Le routage choisi en fonction des paramètres le template à afficher (*films.html* ou *details.html*).
Ces deux templates hérite *base_template.html*.

Vous pouvez accéder à notre travail à l'addresse suivante : [crud films](http://localhost/~ynden/dm-crud-iut-2016/index.php)

## Installer TWIG ##

Télécharger l'utilitaire **Composer** (de préférence dans votre dossier ~/bin) :

```shell
curl -sS https://getcomposer.org/installer | php
```

Naviguez ensuite à la racine du dossier de votre projet puis installez **TWIG** :

```shell
php composer.phar require "twig/twig:~1.0"
```

**TWIG** va ensuite s'installer dans ce dossier.
* composer.lock
* composer.json
* vendor/

## Auteurs ##

Valérian YNDEN & Pierre CHAT
Dépôt de code source : [Bitbucket](https://bitbucket.org/Midi12/dm-crud-iut-2016/src)