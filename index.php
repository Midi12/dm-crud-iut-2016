<?php
include_once 'vendor/autoload.php';
include_once 'php/crud.php';

Twig_Autoloader::register();

try
{
	$loader = new Twig_Loader_Filesystem('templates');
	$twig = new Twig_Environment($loader);
	
	if (!isset($_GET['id']))
	{
		$template = $twig->loadTemplate('films.html');
		
		$beg = 0; $range = 25; //default value
		
		if (isset($_POST['beg']) != null && isset($_POST['range']) != null)
		{
			$beg = $_POST['beg'];
			$range = $_POST['range'];
			
			if (isset($_POST['move']) && $_POST['move'] == '<')
				if ($beg - $range * 2 >= 0)
					$beg -= $range * 2;
				else
					$beg = 0;
		}
		
		echo $template->render(array(
			'films' => get_all_films($beg, $range),
			'beg' => $beg,
			'range'=> $range
			));
	}
	else
	{
		$template = $twig->loadTemplate('detail.html');
		echo $template->render(array(
			'film' => get_all_details($_GET['id']),
			'genres' => get_genres($_GET['id']),
			'real' => get_realisateur($_GET['id']),
			'acteurs' => get_acteurs($_GET['id'])
			));
	}
}
catch(Exception $e)
{
	die ('ERROR: ' . $e->getMessage());
}
?>
