<?php
	require_once("crud.php");
	if (isset($_POST['action']) && (isset($_POST['id']) || isset($_POST['titre_o'])))
	{
		if ($_POST['action'] == 'removefilm')
		{
			remove_film($_POST['id']);
		}
		else if ($_POST['action'] == 'removeact')
		{
			remove_act($_POST['id'], $_POST['aid']);
		}
		else if ($_POST['action'] == 'removereal')
		{
			remove_real($_POST['id']);
		}
		else if ($_POST['action'] == 'addreal')
		{
			add_real($_POST['id'], $_POST['nom'], $_POST['prenom'], $_POST['nat'], $_POST['naiss'], $_POST['deces']);
		}
		else if ($_POST['action'] == 'addfilm')
		{
			add_film($_POST['titre_orig'], $_POST['titre_fr'], $_POST['annee'], $_POST['pays'], $_POST['genre'], $_POST['duree'], $_POST['couleur']);
		}
		else if ($_POST['action'] == 'addact')
		{
			add_act($_POST['id'], $_POST['nom'], $_POST['prenom'], $_POST['nat'], $_POST['naiss'], $_POST['deces']);
		}
		else if ($_POST['action'] == 'modfilm' && isset($_POST['field']) && isset($_POST['value']))
		{
			modif_film($_POST['id'], $_POST['field'], $_POST['value']);
		}
		else if ($_POST['action'] == 'newfilm' && isset($_POST['titre_o']) && isset($_POST['titre_fr']) && isset($_POST['pays']) && isset($_POST['date']))
		{
			$newid = new_film($_POST['titre_o'], $_POST['titre_fr'], $_POST['pays'], $_POST['date']);
			header('Location: index.php?id=' . $newid);
		}
		else
		{
			die("error unk action");
		}
	}
	
	header('Location: '.$_SERVER['HTTP_REFERER']);
?>
