<?php
require_once("connexion.php");

function get_all_films($beg, $range)
{
	$db = connect_bd();
	$q = "SELECT * FROM films WHERE code_film >= " . $beg . " AND code_film < " . ($range + $beg);
	return $db->query($q)->fetchAll();
}

function get_all_details($id)
{
	$db = connect_bd();
	$q = "SELECT * FROM films WHERE code_film = " . $id;
	return $db->query($q)->fetch();
}

function get_genres($id)
{
	$db = connect_bd();
	$q = "SELECT DISTINCT nom_genre FROM genres, classification WHERE ref_code_film = " . $id . " AND code_genre = ref_code_genre";
	return $db->query($q)->fetchAll();
}

function get_realisateur($id)
{
	$db = connect_bd();
	$q = "SELECT individus.* FROM films, individus WHERE code_indiv = realisateur AND code_film = " . $id;
	return $db->query($q)->fetch();
}

function get_acteurs($id)
{
	$db = connect_bd();
	$q = "SELECT DISTINCT individus.* FROM acteurs, individus WHERE ref_code_film = " . $id . " AND code_indiv = ref_code_acteur";
	return $db->query($q)->fetchAll();
}

function remove_film($id)
{
	$db = connect_bd();
	$q = "DELETE FROM acteurs WHERE ref_code_film=" . $id;
	$db->query($q);
	$q = "DELETE FROM classification WHERE ref_code_film=" . $id;
	$db->query($q);
	$q = "DELETE FROM films WHERE code_film=" . $id;
	$db->query($q);
}

function add_film($titre_orig, $titre_fr, $annee, $pays, $genre, $duree, $couleur)
{
	$db = connect_bd();
	$num = $db->query("SELECT * FROM films ORDER BY code_film DESC LIMIT 1")->fetch()['code_film'] + 1;
	$q = "INSERT INTO films VALUES(". $num .",". $titre_orig .",". $titre_fr .",". $pays .",". $annee .",". $duree .",". $couleur .", 0, NULL)";
}

function add_act($fid, $nom, $prenom, $nat, $naiss, $deces)
{
   	$db = connect_bd();
	$q_act = "SELECT * FROM individus WHERE nom='" . $nom . "' AND prenom='" . $prenom . "'";
	
	$res_q_act = $db->query($q_act);
	
	if ($res_q_act)
	{
		$aid = $res_q_act->fetch()['code_indiv'];
	}
	else
	{
		$aid = $db->query("SELECT * FROM individus ORDER BY code_indiv DESC LIMIT 1")->fetch()['code_indiv'] + 1;
		$q_ins_act = "INSERT INTO individus VALUES(" . $aid . ", '" . $nom . "', '" . $prenom . "', '" . $nat . "', " . $naiss . ", " . $deces . ")" ;
		if(!$db->query($q_ins_act))
			die("not inserted");
	}
	 
	$q_films = "INSERT INTO acteurs VALUES(" . $fid . ", " . $aid . ")";
	if(!$db->query($q_films))
		die("not linked");
}

function remove_act($id, $aid)
{
	$db = connect_bd();
	$q = "DELETE FROM acteurs WHERE ref_code_film=" . $id . " AND ref_code_acteur=" . $aid;
	$db->query($q);
}

function add_real($fid, $nom, $prenom, $nat, $naiss, $deces)
{
    $db = connect_bd();
	$q_real = "SELECT individus.* FROM individus WHERE nom='" . $nom . "' AND prenom='" . $prenom . "'";
	
	$res_q_real = $db->query($q_real);
	
	if ($res_q_real)
	{
		$rid = $res_q_real->fetch()['code_indiv'];
	}
	else
	{
		$rid = $db->query("SELECT * FROM individus ORDER BY code_indiv DESC LIMIT 1")->fetch()['code_indiv'] + 1;
		$q_ins_real = "INSERT INTO individus VALUES(" . $rid . ", '" . $nom . "', '" . $prenom . "', '" . $nat . "', " . $naiss . ", " . $deces . ")" ;
		if(!$db->query($q_ins_real))
			die("not inserted");
	}
	 
	$q_films = "UPDATE films SET realisateur=" . $rid . " WHERE code_film=" . $fid;
	if(!$db->query($q_films))
		die("not linked");
}

function remove_real($id)
{
	$db = connect_bd();
	$q = "UPDATE films SET realisateur=0 WHERE code_film=" . $id;
	$db->query($q);
}

function modif_film($id, $field, $value)
{
	$db = connect_bd();
	$q = "UPDATE films SET " . $field . "='" . $value . "' WHERE code_film=" . $id;
	if (!$db->query($q))
		die("not modified : ".$q);
}

function new_film($titre_o, $titre_fr, $pays, $date)
{
	$db = connect_bd();
	
	$newid = $db->query("SELECT * FROM films ORDER BY code_film DESC LIMIT 1")->fetch()['code_film'] + 1;
		
	$q_ins_film = "INSERT INTO films VALUES(" . $newid . ", '" . $titre_o . "', '" . $titre_fr . "', '" . $pays . "', " . $date . ", 0, 'couleur', 0, 'default.jpg')";
	
	if(!$db->query($q_ins_film))
		die("not inserted : " . $q_ins_film);
		
	return $newid;
}
?>
